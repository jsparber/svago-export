extern crate argparse;
extern crate cairo;
extern crate librsvg;
extern crate svgcleaner;
extern crate xml;

use argparse::{ArgumentParser, Store, StoreFalse, StoreTrue};

use cairo::SvgUnit;
use librsvg::CairoRenderer;
use librsvg::Loader;
use librsvg::SvgHandle;

use std::fs::create_dir;
use std::fs::remove_dir;
use std::fs::File;
use std::io::BufReader;

use xml::reader::{EventReader, XmlEvent};

#[derive(Debug, Clone)]
pub struct Coord {
    x: f64,
    y: f64,
    width: f64,
    height: f64,
    name: String,
}

#[derive(Debug, Clone)]
pub struct Icon {
    name: String,
    id: String,
    group: String,
    square: String,
    /* We need to store the class and the color so we can readd the class to the objects based on
     * the color (Class, Color) */
    class: Vec<(String, String)>,
}

fn main() {
    let mut verbose = false;
    let mut enable_cleaner = true;
    let mut output_path = String::from("./output");
    let mut path = String::from("");
    {
        // this block limits scope of borrows by ap.refer() method
        let mut ap = ArgumentParser::new();
        ap.set_description("Export SVGs from a stencils SVG file.");
        ap.refer(&mut path)
            .add_argument("input", Store, "SVG stencil")
            .required();
        ap.refer(&mut verbose)
            .add_option(&["-v", "--verbose"], StoreTrue, "Be verbose");
        ap.refer(&mut output_path)
            .add_option(&["-o", "--output"], Store, "Output directory");
        ap.refer(&mut enable_cleaner).add_option(
            &["--no-clean"],
            StoreFalse,
            "Don't remove unnecessary data (disable svgcleaner)",
        );
        ap.parse_args_or_exit();
    }

    let file = File::open(path.clone());
    if let Err(file) = file {
        print!("{}", file.to_string());
        return;
    }
    let file = file.unwrap();
    let file = BufReader::new(file);

    let parser = EventReader::new(file);
    let mut current: Option<(i32, Icon)> = None;
    let mut elements: Vec<Icon> = vec![];
    let mut level = 0;
    let mut item = Icon {
        group: String::from(""),
        name: String::from(""),
        id: String::from(""),
        square: String::from(""),
        class: vec![],
    };

    for e in parser {
        match e {
            Ok(XmlEvent::StartElement {
                name, attributes, ..
            }) => {
                level = level + 1;
                let mut found = false;
                if name.local_name == "g" {
                    for a in attributes.clone() {
                        /* Find group name */
                        if a.name.local_name == "groupmode" {
                            if a.value == "layer" {
                                found = true;
                            }
                        }
                        if found && a.name.local_name == "label" {
                            item.group = a.value.clone();
                            current = Some((level, item.clone()));
                        }

                        if let Some((ref index, ref mut item)) = current {
                            if level == index + 1 {
                                /* Find group elements */
                                if a.name.local_name == "label" {
                                    item.name = a.value.clone();
                                }
                                if a.name.local_name == "id" {
                                    item.id = a.value.clone();
                                }
                            }
                        }
                    }
                }
                if let Some((ref index, ref mut item)) = current {
                    if level == index + 2 {
                        let color = attributes
                            .iter()
                            .find(|a| a.name.local_name == "style")
                            .map(|a| {
                                a.value
                                    .split(";")
                                    .find(|value| {
                                        value.contains("fill:") && !value.contains("none")
                                    })
                                    .map(|s| s.trim_start_matches("fill:").to_string())
                            })
                            .map_or(None, |a| a);
                        let class = attributes
                            .iter()
                            .find(|a| a.name.local_name == "class")
                            .map(|a| a.value.to_string());
                        if let Some(class) = class {
                            if let Some(color) = color {
                                item.class.push((class, color))
                            }
                        }

                        /* get 16x16 square, workaround to get the real position of the icon
                         * because rsvg dosen't return the right size and position for some icons */
                        if name.local_name == "rect" {
                            let mut found = 0;
                            let mut rect = None;
                            for a in attributes.clone() {
                                /* Find the id for the 16x16 rect */
                                if a.name.local_name == "width"
                                    && a.value.parse::<f64>().unwrap_or(0f64).round() == 16f64
                                {
                                    found = found + 1;
                                }
                                if a.name.local_name == "height"
                                    && a.value.parse::<f64>().unwrap_or(0f64).round() == 16f64
                                {
                                    found = found + 1;
                                }
                                if a.name.local_name == "id" {
                                    rect = Some(a.value);
                                }
                            }
                            if found == 2 {
                                if let Some(id) = rect {
                                    item.square = id;
                                }
                            }
                        }
                    }
                }
            }
            Ok(XmlEvent::EndElement { name }) => {
                if name.local_name == "g" {
                    if let Some((index, element)) = current.clone() {
                        if index + 1 == level {
                            elements.push(element.clone());
                            /* empty the current item */
                            current = Some((index, item.clone()));
                        }
                    }
                }
                level = level - 1;
            }
            Err(e) => {
                println!("Error: {}", e);
                break;
            }
            _ => {}
        }
    }

    /* TODO: Show error message for not empty directories */
    let _ = remove_dir(output_path.clone());
    let _ = create_dir(output_path.clone());

    let handle = Loader::new().read_path(&path);
    if let Err(ref e) = handle {
        println!("{}", e);
    }

    let handle = handle.expect("Input file couldn't be read");

    for item in elements.iter() {
        if validate_item(item, verbose) {
            /* create_dir will fail if the directory already exsists */
            let _ = create_dir(format!("{}/{}", output_path, item.group));
            let file_path = format!(
                "{}/{}/{}",
                output_path,
                item.group,
                create_file_name(&item.name)
            );
            /* Render return false if the file wasn't rendered */
            if render(&handle, item, &file_path) {
                if enable_cleaner {
                    clean(&file_path);
                }

                for class in item.class.iter() {
                    println!(
                        "We need to add a class {} to {} with base on color {}",
                        class.0, item.name, class.1
                    );
                }
            }
        }
    }
    println!("The rendered icons are in {}", output_path);
}

/* We need to treath rtl icons special */
fn create_file_name(name: &String) -> String {
    if name.ends_with("rtl") {
        format!("{}-symbolic-rtl.svg", name.trim_end_matches("-rtl"))
    } else {
        format!("{}-symbolic.svg", name)
    }
}

fn validate_item(item: &Icon, v: bool) -> bool {
    if item.name == "" || item.id == "" || item.group == "" {
        println!(
            "FIXME: {:?} As some information missing, icon ignored",
            item
        );
        return false;
    }
    if item.square == "" {
        println!("FIXME: {} has no 16x16 square, icon ignored", item.name);
        return false;
    }
    if item.name.ends_with("-old") {
        if v {
            println!("Warning: {} contains 'old', icon ignored", item.name);
        }
        return false;
    }
    if item.name.ends_with("-alt") {
        if v {
            println!("Warning: {} contains 'alt', icon ignored", item.name);
        }
        return false;
    }
    return true;
}

fn render(handle: &SvgHandle, item: &Icon, file: &String) -> bool {
    let group = format!("#{}", item.id);
    let rect = format!("#{}", item.square);
    let renderer = CairoRenderer::new(handle);
    /* FIXME: vbox of intrinsic_dimensions() is for some reason None */
    let viewport = {
        let doc = renderer.intrinsic_dimensions();

        cairo::Rectangle {
            x: 0.0,
            y: 0.0,
            width: doc.width.unwrap().length,
            height: doc.height.unwrap().length,
        }
    };

    let (rect, _) = renderer
        .geometry_for_layer(Some(&rect), &viewport)
        .unwrap();

    /* don't render icons outside the viewport */
    if rect.x < 0. || rect.y < 0. || rect.x > viewport.width || rect.y > viewport.height {
        return false;
    }

    let mut surface = cairo::SvgSurface::new(rect.width, rect.height, file);
    surface.set_document_unit(SvgUnit::Px);
    let cr = cairo::Context::new(&surface);
    cr.translate(-rect.x, -rect.y);

    let _ = renderer.render_layer (&cr, Some(group.as_str()), &viewport);
    true
}

fn clean(file: &String) -> Option<()> {
    let options = svgcleaner::CleaningOptions {
        remove_unused_defs: true,
        convert_shapes: true,
        remove_title: true,
        remove_desc: true,
        remove_metadata: true,
        remove_dupl_linear_gradients: true,
        remove_dupl_radial_gradients: true,
        remove_dupl_fe_gaussian_blur: true,
        ungroup_groups: true,
        ungroup_defs: true,
        group_by_style: true,
        merge_gradients: true,
        regroup_gradient_stops: true,
        remove_invalid_stops: true,
        remove_invisible_elements: true,
        resolve_use: true,
        remove_version: true,
        remove_unreferenced_ids: true,
        trim_ids: true,
        remove_text_attributes: true,
        remove_unused_coordinates: true,
        remove_default_attributes: true,
        remove_xmlns_xlink_attribute: true,
        remove_needless_attributes: true,
        remove_gradient_attributes: true,
        join_style_attributes: svgcleaner::StyleJoinMode::None,
        apply_transform_to_gradients: true,
        apply_transform_to_shapes: true,

        paths_to_relative: true,
        remove_unused_segments: true,
        convert_segments: true,
        apply_transform_to_paths: true,

        coordinates_precision: 6,
        properties_precision: 6,
        paths_coordinates_precision: 8,
        transforms_precision: 8,
    };
    let data = svgcleaner::cleaner::load_file(file);
    if let Err(ref e) = data {
        panic!("\nFile: {}\n{}", file, e);
    }
    let data = data.unwrap();
    let mut document =
        svgcleaner::cleaner::parse_data(&data, &svgcleaner::ParseOptions::default()).unwrap();
    let _ = svgcleaner::cleaner::clean_doc(
        &mut document,
        &options,
        &svgcleaner::WriteOptions::default(),
    );
    let mut buf = vec![];
    svgcleaner::cleaner::write_buffer(&document, &svgcleaner::WriteOptions::default(), &mut buf);
    let _ = svgcleaner::cleaner::save_file(&buf, file);
    return None;
}
