# SVAGO-export

App to export the gnome-stencils.svg file. Written in rust.

## Instructions
```sh
git clone https://gitlab.gnome.org/jsparber/svago-export.git && cd svago-export
# build and install the flatpak
flatpak-builder --install --force-clean app flatpak/org.gnome.SvagoExport.json
# Get adwaita icon theme
git clone https://gitlab.gnome.org/GNOME/adwaita-icon-theme.git
# Run SVAGO-export
flatpak run org.gnome.SvagoExport adwaita-icon-theme/src/symbolic/gnome-stencils.svg
```
